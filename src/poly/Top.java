package poly;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.lang.System.out;

public class Top {

  static public final class List<T> {
    final T head;
    final List<T> tail;
      
    public List(T head, List<T> tail) {
      this.head = head;
      this.tail = tail;
    }
    
    public String toString() {
      return str(this);
    }
  }
  
  static <T> List<T> cons(T x, List<T> xs) {
    return new List<T>(x,xs);
  }
  
  @SafeVarargs
  static <T> List<T> list(T... xs) {
    List<T> r = null;
    for (int i=xs.length-1;i>=0;i--)
      r = cons(xs[i],r);
    return r;
  }
  
  static <T> List<T> reverse(List<T> x) {
    List<T> r = null;
    while (x != null) {
      r = cons(x.head,r);
      x = x.tail;
    }
    return r;
  }
  
  static <A,B> List<B> reverse_map(Function<A,B> f, List<A> x) {
    List<B> r = null;
    while (x != null) {
      r = cons(f.apply(x.head),r);
      x = x.tail;
    }
    return r;
  }
  
  static <A,B> List<B> map(Function<A,B> f, List<A> x) {
    return reverse(reverse_map(f,x));
  }
  
  static <A,B> A fold(BiFunction<A,B,A> f, A s, List<B> x) {
    while (x != null) {
      s = f.apply(s,x.head);
      x = x.tail;
    }
    return s;
  }
  
  static <A> void iter(Consumer<A> f, List<A> x) {
    while (x != null) {
      f.accept(x.head);
      x = x.tail;
    }
  }
  
  static <T> String str(List<T> x) {
    StringBuffer s = new StringBuffer();
    iter(y -> { s.append(','); s.append(y.toString()); }, x);
    s.setCharAt(0,'[');
    s.append(']');
    return s.toString();
  }
  
  static List<Integer> test_closure() {
    List<Function<Integer,Integer>> fs = null;
    for (int i=0;i<10;i++) {
      final int ii = i; // Needed since i is not final or effectively final.
      fs = cons(x -> x+ii,fs);
    }  
    return map(f -> f.apply(-5),fs);
  }
  
  public static void main(String[] args) {
    // x = 1,2,3
    // y = 2*x
    // n = [1,2],[3,4,5]
    // s = map sum n
    // print 'x = ' x
    // print 'y = ' y
    // print 's = ' s
    List<Integer> x = list(1,2,3);
    List<Integer> y = map(i -> 2*i,x);
    List<List<Integer>> n = list(list(1,2),list(3,4,5));
    BiFunction<Integer,Integer,Integer> f = (a,b) -> a+b;
    Function<List<Integer>,Integer> ff = (List<Integer> l) -> fold(f,0,l);
    List<Integer> s = map(ff,n);
    out.println("x = "+str(x));
    out.println("y = "+str(y));
    out.println("s = "+s);
    out.println("test_closure = "+str(test_closure()));
  }
}
